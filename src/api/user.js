/**
 * @description API for user name.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 30.11.2017
 */

const express = require("express");
const router = express.Router();
const user = require('./../db/user.js');

// get user name
var get = function (req, res) {
	res.json({
		"user": req.session.user.jmeno,
		"role": req.session.user.role
	});
}

// get all users
var getAllUsers = function (req, res) {

	user.getAllUsers(req.session.user.sklad)
	.then(data => {
		res.json(
			data
		);
	})
	.catch(e => {
		// console.log(e);
		res.status(500).send(e);
	});
}

// get all users
var update = function (req, res) {
	
	// array
	if (req.body.data && req.body.remove) {
		
		if (req.body.remove.length != 0) {
			// delete and update
			return user.deleteUser(req.body.remove)
			.then(rows => {
				return user.update(req.body.data);
			})
			.then(data => {
				res.json(
					data
				);
			})
			.catch(e => {
				res.status(500).send(e);
			});
		// only update
		} else {
			user.update(req.body.data)
			.then(data => {
				res.json(
					data
				);
			})
			.catch(e => {
				res.status(500).send(e);
			});
		}
		
	} else {
		res.status(400).send("Bad request");
	}
}

// add all users
var add = function (req, res) {
	
	// array
	if (req.body.data) {
		user.add(req.body.data, req.session.user.sklad)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			console.log(e);
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

/**
 * @summary Get all records
 * @function records
 * @private
 */
router.get("/", get);


router.get("/all", getAllUsers);

router.post("/update", update);

router.post("/add", add);

module.exports = router;
