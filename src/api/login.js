const express = require("express");
const router = express.Router();
const login = require('./../db/login.js');
const session = require('express-session');

// get all drogs
var getLogin = function (req, res) {

	if (req.body.user && req.body.password) {
		// all drog information
		login.tryLogin(req.body.user, req.body.password)
		.then(user_data => {
			req.session.user = user_data;
			req.session.save();

			return res.status(200).json(user_data);
		})
		.catch(e => {
			res.status(e.status).send(e.msg);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

// get all drogs
var logout = function (req, res) {
	
	req.session.destroy();
	req.session = null;

	return res.status(200).json("deleted cookie");
}


router.post("/", getLogin);

router.get("/logout", logout);

module.exports = router;