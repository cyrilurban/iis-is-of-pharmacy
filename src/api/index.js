/**
 * @description index for API.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 20.11.2017
 */

const express = require("express");
const router = express.Router();

router.use("/", require("./drog.js"));
router.use("/records", require("./records.js"));
router.use("/insurance", require("./insurance.js"));
router.use("/subsidy", require("./subsidy.js"));
router.use("/login", require("./login.js"));
router.use("/user", require("./user.js"));

module.exports = router;
