/**
 * @description API for improt subsidy.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 23.11.2017
 */

const express = require("express");
const router = express.Router();
const subsidy = require('./../db/subsidy.js');
const drog = require('./../db/drog.js');

// get all drogs
var getAllDrog = function (req, res) {
	// all drog
	drog.getAllDrog()
	.then(data => {
		res.json(
			data
		);
	})
	.catch(e => {
		res.status(500).send(e);
	});
}

// get one subsidy for drog
var getOneSubsidy = function (req, res) {
	// check existence of necessery attribtus
	if (req.body.id_pojistovna && req.body.id_lek_info) {
		// get one subsidy
		drog.getSubsidy(req.body.id_pojistovna, req.body.id_lek_info)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

// update N subsidy for drog
var updateSubsidy = function (req, res) {
	// check existence of array lek and necessery attribtus
	if (req.body.lek && req.body.lek instanceof Array && 
		req.body.lek.length != 0 && req.body.id_pojistovna) {

		// update subsidys
		subsidy.updateSubsidy(req.body.id_pojistovna, req.body.lek)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

router.post("/", getOneSubsidy);

router.post("/update", updateSubsidy);

router.get("/", getAllDrog);

module.exports = router;
