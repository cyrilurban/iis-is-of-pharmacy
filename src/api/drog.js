/**
 * @description API for drog.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 20.11.2017
 */

const express = require("express");
const router = express.Router();
const drog = require('./../db/drog.js');

// get all drogs
var get = function (req, res) {
	// all drog
	drog.getAllDrog()
	.then(data => {
		res.json(
			data
		);
	})
	.catch(e => {
		res.status(500).send(e);
	});
}

// get drog detail base on id_lek_info
var getDrogDetail = function (req, res) {

	// check existence of id
	if (req.body.id_lek_info) {
		// all drog information
		drog.getDrogDetail(req.body.id_lek_info, req.session.user.sklad)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

// get subsidy to drog (with his id_lek_info) from insurance company (with his id_pojistovna)
var getSubsidy = function (req, res) {
	// check existence of ids
	if (req.body.id_pojistovna && req.body.id_lek_info) {
		// all drog information
		drog.getSubsidy(req.body.id_pojistovna, req.body.id_lek_info)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

// post purchase with id of insurance company and with
// array of objects with id of drog and flag if is drog
// is on subsidy 
var postPurchase = function (req, res) {
	// check existence of array lek
	if (req.body.lek && req.body.lek instanceof Array && req.body.lek.length != 0) {
		// no insurance company -> set null
		if (!req.body.id_pojistovna) {
			req.body.id_pojistovna = null;
		}
		// post purchase
		drog.postPurchase(req.body.id_pojistovna, req.body.lek)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}

/**
 * @summary Get all drog
 * @function drog
 * @private
 */
router.get("/", get);

router.post("/", getDrogDetail);

router.post("/add", getSubsidy);

router.post("/purchase", postPurchase);

module.exports = router;
