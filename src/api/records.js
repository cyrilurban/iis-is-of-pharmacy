/**
 * @description API for records.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 23.11.2017
 */

const express = require("express");
const router = express.Router();
const records = require('./../db/records.js');

// get all records
var get = function (req, res) {
	// all records
	records.getAllRecords(req.session.user.sklad)
	.then(data => {
		res.json(
			data
		);
	})
	.catch(e => {
		res.status(400).send(e);
	});
}

/**
 * @summary Get all records
 * @function records
 * @private
 */
router.post("/", get);

module.exports = router;
