/**
 * @description API for insurance export.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 23.11.2017
 */

const express = require("express");
const router = express.Router();
const insurance = require('./../db/insurance.js');

// get all sold drogs and their insurances for insurance
var getInsuranceSubsidy = function (req, res) {
	
	// check existence of necessery attribtus
	if (req.body.id_pojistovna && req.body.from &&
		req.body.to) {
		// no branch -> set * (all)
		if (!req.body.branch) {
			req.body.branch = req.session.user.sklad;
		} else {
			req.body.branch = "*";
		}
		// post purchase
		insurance.getInsuranceSubsidy(req.body.id_pojistovna, 
			req.body.from, req.body.to, req.body.branch)
		.then(data => {
			res.json(
				data
			);
		})
		.catch(e => {
			res.status(500).send(e);
		});
	} else {
		res.status(400).send("Bad request");
	}
}


// get all insurance company (and their ids)
var getAllInsurance = function (req, res) {
	// post purchase
	insurance.getAllInsurance()
	.then(data => {
		res.json(
			data
		);
	})
	.catch(e => {
		res.status(500).send(e);
	});
}

router.post("/", getInsuranceSubsidy);

router.get("/", getAllInsurance);

module.exports = router;
