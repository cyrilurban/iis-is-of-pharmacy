/**
 * @description Functions for geting records from DB for drog.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 23.11.2017
 */

const db = require('./init.js');

var getAllRecords = function (branch) {
	return new Promise(function(resolve, reject) {
		var sql = "SELECT lek.cas_prodeje, lek.id_lek_info, lek.id_lek, lek_info.nazev, lek_info.typ_leku, ";
		sql +=  "lek.id_pojistovna, zdravotni_pojistovna.nazev_pojistovny, lek_info.prodejni_cena, lek.prispeno FROM lek ";
		sql += "JOIN lek_info ON lek.id_lek_info = lek_info.id_lek_info ";
		sql += "LEFT JOIN zdravotni_pojistovna ON lek.id_pojistovna = zdravotni_pojistovna.id_pojistovna ";
		sql += "LEFT JOIN prispevek ON lek.id_lek_info = prispevek.id_lek_info AND lek.id_pojistovna = prispevek.id_pojistovna ";
		sql += "WHERE cas_prodeje IS NOT NULL ";
		sql += (branch == '*' ? "" : "AND lek.id_sklad = " + branch + " ");
		sql += "ORDER BY cas_prodeje DESC;";

		db.query(sql, function (err, result) {
			if (err) {
				reject(err);
			}
			
			resolve(result);
		});
	});
};

module.exports = {
	getAllRecords: getAllRecords
};
