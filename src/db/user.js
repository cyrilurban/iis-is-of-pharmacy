/**
 * @description Functions for geting and posting data from DB for user.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 1.12.2017
 */

const db = require('./init.js');

const bcrypt = require("bcrypt");

var getAllUsers = function (id_sklad) {
	return new Promise(function(resolve, reject) {
		var sql = "SELECT id_uzivatel, jmeno, role FROM uzivatel WHERE sklad = " + id_sklad + ";";
		db.query(sql, function (err, result) {
			if (err) {
				reject(err);
			}

			// add column value - concanate name and id (EAN)
			for (var i = 0; i < result.length; i++) {
				result[i].value = result[i].nazev + " - " + result[i].id_lek_info;
			}

			resolve(result);
		});
	});
};

var update = function (data) {
	return new Promise(function(resolve, reject) {

		// variables for sql query 
		var result_sql = "";
		var sql = "UPDATE uzivatel SET role = ? WHERE id_uzivatel = ?;";
		var sql_vars = [];

		// prepare query (and sql_vars)
		for (var i = 0; i < data.length; i++) {
			// update result query
			result_sql += sql;

			// set query variables
			sql_vars.push(data[i].role);
			sql_vars.push(data[i].id);
		}
	
		// send query
		db.query(result_sql, sql_vars, function (err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

var deleteUser = function (data) {
	return new Promise(function(resolve, reject) {

		// variables for sql query 
		var result_sql = "";
		var sql = "DELETE FROM uzivatel WHERE id_uzivatel = ?;";
		var sql_vars = [];

		// prepare query (and sql_vars)
		for (var i = 0; i < data.length; i++) {
			// update result query
			result_sql += sql;

			// set query variables
			sql_vars.push(data[i]);
		}
	
		// send query
		db.query(result_sql, sql_vars, function (err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

var add = function (data, branch) {
	return new Promise(function(resolve, reject) {

		// variables for sql query 
		var result_sql = "";
		// INSERT INTO uzivatel VALUES (NULL, "userpraha", "$2a$10$yiiT8CJcl/SMA6HqTsp/KeB8g7dN6Zsjp3bH2fvXPdvnD3Pr49Tnu", 1, "user");
		var sql = "INSERT INTO uzivatel VALUES (NULL, ?, ?, ?, ?);";
		var sql_vars = [];

		// prepare query (and sql_vars)
		for (var i = 0; i < data.length; i++) {
			// update result query
			result_sql += sql;

			// set query variables
			sql_vars.push(data[i].name);
			
			var hash = bcrypt.hashSync(data[i].pass, 10);
			sql_vars.push(hash);

			sql_vars.push(branch);
			sql_vars.push(data[i].role);
		}
	
		// send query
		db.query(result_sql, sql_vars, function (err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

module.exports = {
	getAllUsers: getAllUsers,
	update: update,
	deleteUser: deleteUser,
	add: add
};
