/**
 * @description Functions for user's login.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 30.11.2017
 */

const db = require('./init.js');
const bcrypt = require('bcrypt');

var tryLogin = function (user, password) {
	
	return new Promise(function(resolve, reject) {
		var sql = "SELECT * ";
		sql +=  "FROM uzivatel ";
		sql += "WHERE jmeno = ? ";

		db.query(sql, [user], function (err, result) {
			if (err) {
				reject(err);
			}
			
			if (result.length == 0) {
				var err = {
					"status": 403,
					"msg": "Nesprávné uživatelské jméno nebo heslo."
				};
				return reject(err);
			}

			bcrypt.compare(password, result[0].heslo, function(err, res) {
				if (res == false) {
					var err = {
						"status": 403,
						"msg": "Nesprávné uživatelské jméno nebo heslo."
					};
					return reject(err);
				} else {
					var user = {
						"jmeno": result[0].jmeno,
						"sklad": result[0].sklad,
						"role": result[0].role
					}
					return resolve(user);
				}
			});
		});
	});
};

module.exports = {
	tryLogin: tryLogin
};
