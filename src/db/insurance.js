/**
 * @description Functions for insurance export.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 23.11.2017
 */

const db = require('./init.js');

var getInsuranceSubsidy = function (id_pojistovna, from, to, branch) {
	return new Promise(function(resolve, reject) {
		/*
		SELECT lek_detail.id_lek_info, lek_detail.nazev, lek_detail.typ_leku, COUNT(*) AS count, prispevek.prispovana_cena
		FROM prispevek
		JOIN
			(SELECT lek.id_lek, lek.id_lek_info, lek_info.nazev, lek_info.typ_leku, lek.cas_prodeje
			FROM lek
			JOIN lek_info on lek.id_lek_info = lek_info.id_lek_info
			WHERE lek.cas_prodeje IS NOT null
			AND lek.id_sklad = 1 --jen nekdy
			AND lek.id_pojistovna = 1
			AND lek.cas_prodeje BETWEEN '2010-01-30' AND '2018-09-29')
		AS lek_detail on prispevek.id_lek_info = lek_detail.id_lek_info
		AND prispevek.id_pojistovna = 1
		GROUP BY id_lek_info
		*/

		var sql = "";
		sql += "SELECT lek_detail.id_lek_info, lek_detail.nazev, lek_detail.typ_leku, COUNT(*) AS count, lek_detail.prispeno ";
		sql += "FROM prispevek ";
		sql += "JOIN ";
		sql += "(SELECT lek.id_lek, lek.id_lek_info, lek_info.nazev, lek_info.typ_leku, lek.cas_prodeje, lek.prispeno ";
		sql += "FROM lek ";
		sql += "JOIN lek_info on lek.id_lek_info = lek_info.id_lek_info ";
		sql += "WHERE lek.cas_prodeje IS NOT NULL ";
		sql += (branch == '*' ? "" : "AND lek.id_sklad = " + branch + " ");
		sql += "AND lek.id_pojistovna = " + id_pojistovna + " ";
		sql += "AND lek.cas_prodeje BETWEEN '" + from + "' AND '" + to +"') ";
		sql += "AS lek_detail on prispevek.id_lek_info = lek_detail.id_lek_info ";
		sql += "AND prispevek.id_pojistovna = " + id_pojistovna + " ";
		sql += "GROUP BY id_lek_info, lek_detail.prispeno";
	
		db.query(sql, function (err, result) {
			if (err) {
				console.log(err);
				reject(err);
			}
			
			resolve(result);
		});
	});
};

var getAllInsurance = function (id_pojistovna, from, to, branch) {
	return new Promise(function(resolve, reject) {
		var sql = "SELECT id_pojistovna, nazev_pojistovny FROM zdravotni_pojistovna";

		db.query(sql, function (err, result) {
			if (err) {
				reject(err);
			}
			
			resolve(result);
		});
	});
};

module.exports = {
	getInsuranceSubsidy: getInsuranceSubsidy,
	getAllInsurance: getAllInsurance
};
