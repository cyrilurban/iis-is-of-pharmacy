/**
 * @description Init DB.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 20.11.2017
 */

const config = require('./../config.js');

/**
 * @summary Init DB, configuration require from config
 * @function knex
 * @static
 */
var mysql = require('mysql').createConnection({
	host: config.conf.db.host,
	user: config.conf.db.user,
	password: config.conf.db.password,
	database: config.conf.db.database,
	multipleStatements: true
});

module.exports = mysql;
