/**
 * @description Functions for geting and posting data from DB for drog.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 20.11.2017
 */

const db = require('./init.js');

var getAllDrog = function () {
	return new Promise(function(resolve, reject) {
		var sql = "SELECT * FROM lek_info";
		db.query(sql, function (err, result) {
			if (err) {
				reject(err);
			}

			// add column value - concanate name and id (EAN)
			for (var i = 0; i < result.length; i++) {
				result[i].value = result[i].nazev + " - " + result[i].id_lek_info;
			}

			resolve(result);
		});
	});
};

var getDrogDetail = function (id_lek_info, id_sklad) {
	return new Promise(function(resolve, reject) {
		
		var sql = "SELECT lek.id_lek FROM lek WHERE id_lek_info = ? AND id_sklad = ? AND cas_prodeje IS NULL";
		
		db.query(sql, [id_lek_info, id_sklad], function (err, result) {
			if (err) {
				reject(err);
			}

			result_obj = {"id_lek": [], "count": 0};

			for (var i = 0; i < result.length; i++) {
				result_obj.id_lek.push(result[i].id_lek);
			}
			result_obj.count = result.length;

			resolve(result_obj);
		});
	});
}

var getSubsidy = function (id_pojistovna, id_lek_info) {
	return new Promise(function(resolve, reject) {
		
		var sql = "SELECT prispovana_cena FROM prispevek WHERE id_pojistovna = ? AND id_lek_info = ?";
		
		db.query(sql, [id_pojistovna, id_lek_info], function (err, result) {
			if (err) {
				reject(err);
			}

			// not subside - return 0 Kc
			if (result.length == 0) {
				result.push({"prispovana_cena": 0});
			}

			resolve(result);
		});
	});
}

var postPurchase = function (id_pojistovna, lek) {
	return new Promise(function(resolve, reject) {

		// variables for sql query 
		var result_sql = "";
		var sql = "UPDATE lek SET id_pojistovna = ?, cas_prodeje = NOW(), prispeno = ? WHERE id_lek = ?;";
		var sql_vars = [];



		// prepare query (and sql_vars)
		for (var i = 0; i < lek.length; i++) {
			// update result query
			result_sql += sql;

			// set first query variable - id_pojistovna
			if (lek[i].predpis === true) {
				sql_vars.push(id_pojistovna);
				sql_vars.push(lek[i].prispeno);
			} else {
				sql_vars.push(null);
				sql_vars.push(null);
			}

			// set second query variable - id_lek
			sql_vars.push(lek[i].id_lek);
		}
	
		// send query
		db.query(result_sql, sql_vars, function (err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
}

module.exports = {
	getAllDrog: getAllDrog,
	getDrogDetail: getDrogDetail,
	getSubsidy: getSubsidy,
	postPurchase: postPurchase
};
