/**
 * @description Functions for manage with subsidy.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 23.11.2017
 */

const db = require('./init.js');

var updateSubsidy = function (id_pojistovna, lek) {
	return new Promise(function(resolve, reject) {

		// variables for sql query 
		var result_sql = "";
		var sql = "UPDATE prispevek SET prispovana_cena = ? WHERE id_pojistovna = ? AND id_lek_info = ?;";
		var sql_vars = [];

		// prepare query (and sql_vars)
		for (var i = 0; i < lek.length; i++) {
			// update result query
			result_sql += sql;

			// set query variables
			sql_vars.push(lek[i].subsidy);
			sql_vars.push(id_pojistovna);
			sql_vars.push(lek[i].id_lek_info);
		}
	
		// send query
		db.query(result_sql, sql_vars, function (err, result) {
			if (err) {
				reject(err);
			}

			resolve(result);
		});
	});
};

module.exports = {
	updateSubsidy: updateSubsidy
};
