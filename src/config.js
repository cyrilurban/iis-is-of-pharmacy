/**
 * @description Config of application.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 20.11.2017
 */

var path = require('path');

/**
 * @summary Config of application. Using path for get filename.
 * @function conf
 * @static
 */
module.exports.conf = {
	"app": {
		"port": 8080
	},
	"db": {
		"host": "localhost",
		"user": "root",
		"password": "1234",
		"database": "pharmacy"
	}
}
