/**
 * @description Init server.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 13.11.2017
 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config.js');
const bcrypt = require('bcrypt');
const session = require('express-session');

// middleware for allow to connect from remote web client
app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000'); // adress, where store client's html (for cookie working)
	res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	next();
});

// set session, using express-session library
app.use(session({
	secret: 'session_cookie_secret',
	// store: sessionStore,
	resave: false,
	saveUninitialized: false
}));

// body parser - accept json request
app.use(bodyParser.json());

// valid session (method OPTION or data from login filtred)
var myLogger = function (req, res, next) {
	if (req.method == "OPTIONS" || req.originalUrl == "/login" || req.session.user) {
		next();
	} else {
		res.status(401).send("Unauthorized");
	}
};
app.use(myLogger);

// valid acces
var acces = function (req, res, next) {
	
	if (req.session.user != undefined && req.originalUrl != "/login" && req.session.user.role == "user") {	 
		var source = req.headers.referer;
		source = source.substr(source.lastIndexOf("/")+1, source.length); // domain (localhost.../)

		// role user and page subsidy or insurance = forbiden acces
		if (source == "subsidy.html" || source == "insurance.html" || 
            source == "adduser.html" || source == "edituser.html") {
			res.status(403).send("Not permission");
		} else {
			next();
		}
	} else {
		next();
	}
};
app.use(acces);

// services
app.use("/", require("./api"));

// listen on port
app.listen(config.conf.app.port);
console.log("Listening on port " + config.conf.app.port);

module.exports = app;
