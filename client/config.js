/**
 * @description Config of client application.
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 30.11.2017
 */

var port = "8080";
var url = "http://localhost:";

// export
var conf = new Object;
conf.url = url+port;