/**
 * @description JS for index.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 25.11.2017
 */

var url = conf.url;
var id_insurance_selected = "null"; // selected insurance id
var drogs; // all lek_info

// selected drog
var selected_drog_info = {
	"name": "",
	"recept": false,
	"id_drog": [],  // array of lek_id from selected lek_info
	"basic_price": "",
	"EAN": ""
};


// this is sync, because we do not work until are all downloaded
$.ajaxSetup({
	async: false, 
	xhrFields: { 
		withCredentials:true 
	}, 
	crossDomain: true
});
$.get(url, function (data, res, response) {
	drogs = data;
});

// auto complete search box
$(function() {
	// setup autocomplete function pulling from drogs[] array
	$('#autocomplete').autocomplete({
		source: drogs,
		select: function (event, ui ) {
			var thehtml = '<strong>Název léku:</strong> ' + ui.item.nazev + 
			' <br> <strong>EAN:</strong> <p id="EAN_el" style="display:inline">' + ui.item.id_lek_info + "</p>" +
			' <br> <strong>Typ léku:</strong> ' + ui.item.typ_leku +
			' <br> <strong>Pouze na předpis:</strong> ' + (ui.item.je_na_predpis == "1" ? "ANO" : "NE") +
			' <br> <strong>Prodejní cena:</strong> <h2 style="margin-top: 3px;">' + ui.item.prodejni_cena + " Kč</h2>";

			$('#outputbox').show();

			$('#outputcontent').html(thehtml);
			checkAvailability(ui.item.id_lek_info);
			
			selected_drog_info.name = ui.item.nazev;
			selected_drog_info.basic_price = ui.item.prodejni_cena;
			selected_drog_info.EAN = ui.item.id_lek_info;
			selected_drog_info.recept = (ui.item.je_na_predpis == "1" ? "ANO" : "NE");
		}
	});		
});

// async post for check availability on this "id_sklad"
var checkAvailability = function (id_lek_info) {
	$.ajax ({
		url: url,
		type: "POST",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({"id_lek_info": id_lek_info}),
		success: function(data, nothing, response) {
			if(response.status == 200) {
				var result_count = data.count;
				var row_count = $('#table tbody tr').length;
				var EAN_current = $('#EAN_el').text();
				var EAN_table;

				for (var i = 0; i < row_count; i++) {
					EAN_table = $('table tbody tr').eq(i).find('td').eq(0).html();

					if (EAN_table == EAN_current) {
						var current_count = parseInt($('table tbody tr').eq(i).find('td').eq(3).html(),10);
						result_count = result_count - current_count;
					}
				}

				$('#count').html("<strong> Skladem na této pobočce: </strong><h4 style='display:inline' id='count_elm'>" + result_count + " ks</h4>");
				selected_drog_info.id_drog = data.id_lek;

			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
		}
	});
};

// async get for check get All Insurance
var insurance_company = [];
var getAllInsurance = function () {
	$.ajax ({
		url: url + "/insurance",
		type: "GET",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		success: function(data, nothing, response, mydata) {
			if(response.status == 200) {
				insurance_company = data;
			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (response.status == 401) {
				window.location.href="login.html";
			}
		}
	});
};
getAllInsurance();

// fill the select downloaded data
$(function() {
	for (var i = 0; i < insurance_company.length; i++) {
		$('#insurances')
		.append($("<option></option>")
		.attr("value", insurance_company[i].id_pojistovna)
		.text(insurance_company[i].nazev_pojistovny)); 
	}
});

// select the insurance company and write its to the label top on drog list
$(function() {
	$( "select" )
	.change(function () {
		
		var row_count = $('#table tbody tr').length;   
		if ($("select option:selected").val() == null || $("select option:selected").val() == "null") {
			var on_recept;
			for (var i = 0; i < row_count; i++) {
				on_recept = $('table tbody tr').eq(i).find('td').eq(2).html();
				if (on_recept == "ANO") {
					alert("Chyba! Nelze zvolit žádnou pojišťovnu, když je vybrán lék s předpisem.");
					
					$("#insurances").val(id_insurance_selected);
					return;
				}
			}
		}

		var str = "";
		$("select option:selected").each(function() {
			str = $(this).text();
			id_insurance_selected = $(this).val();
		});

		$('#insurance_selected').html("Zákazníkova pojišťovna: " + str);

		// update table (new insurance = new subsidies and new sum)
		var drog_EAN;
		for (var i = 0; i < row_count; i++) {
			drog_EAN = $('table tbody tr').eq(i).find('td').eq(0).html();
			getSubsidyAndSum(drog_EAN, i);
		}

		updateTotalSum();
	})
	.change();
});

$(function() {
	$("#add").click(function() {
		if (valid()) {
			if(!merge()) {
				addToDrogLists();
			}
			updateTotalSum();
			cleaneSelect();
		}
	});
});

// merge two drog and add. their count
var merge = function () {
	var EAN_current = $('#EAN_el').text();
	var recept_current = ($("#recept_check").is(':checked')) ? "ANO" : "NE";
	var EAN_table;
	var recept_table;
	var used_drog_id = [];

	var row_count = $('#table tbody tr').length;
	for (var i = 0; i < row_count; i++) {
		EAN_table = $('table tbody tr').eq(i).find('td').eq(0).html();
		recept_table = $('table tbody tr').eq(i).find('td').eq(2).html();

		if (EAN_table == EAN_current && recept_table == recept_current) {
			var drog_want = parseInt($('#drog_count').val(), 10);
			var current_count = parseInt($('table tbody tr').eq(i).find('td').eq(3).html(),10);
			// merge and increment count
			$('table tbody tr').eq(i).find('td').eq(3).html(drog_want+current_count);
			// recalculate sum
			calculateSum(i);

			// find what id can use
			for (var j = 0; j < row_count; j++) {
				EAN_table = $('table tbody tr').eq(j).find('td').eq(0).html();
				if (EAN_table == EAN_current) {
					var cur_arr = ($('table tbody tr').eq(j).find('td').eq(7).html()).split(',').map(function(el){ return +el;});
					// allready used
					used_drog_id = used_drog_id.concat(cur_arr);
				}
			}
			var can_use_drog_id = $(selected_drog_info.id_drog).not(used_drog_id).get();

			var old_drog_id = ($('table tbody tr').eq(i).find('td').eq(7).html()).split(',').map(function(el){ return +el;});

			var new_id; 
			for (var x = 0; x < drog_want; x++) {
				new_id = can_use_drog_id.shift();
				old_drog_id.push(new_id);
			}

			// set merge
			$('table tbody tr').eq(i).find('td').eq(7).html(old_drog_id.toString());

			return true;
		}
	}
	return false
}

// add one drog to drog list
var rowCount = 0;
var addToDrogLists = function () {
	// get variables
	var recept = ( $("#recept_check").is(':checked') ) ? "ANO" : "NE";
	var drog_count = ( $("#drog_count").val());
	var result_id_drog = [];
	var cur_id_drog;

	// find what drog_id can use
	var used_drog_id = [];
	var EAN_current = $('#EAN_el').text();
	var EAN_table;
	var row_count = $('#table tbody tr').length;
	for (var j = 0; j < row_count; j++) {
		EAN_table = $('table tbody tr').eq(j).find('td').eq(0).html();
		if (EAN_table == EAN_current) {
			var cur_arr = ($('table tbody tr').eq(j).find('td').eq(7).html()).split(',').map(function(el){ return +el;});
			// allready used
			used_drog_id = used_drog_id.concat(cur_arr);
		}
	}
	var can_use_drog_id = $(selected_drog_info.id_drog).not(used_drog_id).get();

	// use drog id
	for(var i = 0; i < drog_count; i++) {
		cur_id_drog = can_use_drog_id.shift();
		result_id_drog.push(cur_id_drog);
	}

	// prepare html to table
	var newRowContent = "<tr><td>" + selected_drog_info.EAN + "</td><td>" + selected_drog_info.name + "</td><td>" + recept;
	newRowContent += "</td><td>" + parseInt(drog_count, 10) + "</td><td>" + selected_drog_info.basic_price + " Kč</td><td>";
	newRowContent += "</td><td>" + "0" + " Kč</td><td hidden='true'>" + result_id_drog + "</td></tr>";
	// add row to table
	$("#table tbody").append(newRowContent);

	getSubsidyAndSum(selected_drog_info.EAN, rowCount);

	rowCount++;
}

// reset and clean after add drog
var cleaneSelect = function () {
	$("#autocomplete").val("");
	$("#drog_count").val("1");
	// Unchecks it
	$('#recept_check').attr('checked', false);
	// add row to table
	$('#outputbox').hide();
}

// async get for check get All Insurance
var getSubsidyAndSum = function (id_lek_info, position) {
	$.ajax ({
		url: url + "/add",
		type: "POST",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({"id_lek_info": id_lek_info, "id_pojistovna": id_insurance_selected}),
		success: function(data, nothing, response) {
			if(response.status == 200) {
				
				var recept = $('table tbody tr').eq(position).find('td').eq(2).html();

				if (recept == "NE") {
					$('table tbody tr').eq(position).find('td').eq(5).html("0 Kč");
				} else {
					$('table tbody tr').eq(position).find('td').eq(5).html(data[0].prispovana_cena + " Kč");
				}

				calculateSum(position);
			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
		}
	});
};

// calculate result sum price for one row
var calculateSum = function (row) {
	var result_price;
	var recept = $('table tbody tr').eq(row).find('td').eq(2).html();
	var count = parseInt($('table tbody tr').eq(row).find('td').eq(3).html(), 10);
	var basic_price = parseInt($('table tbody tr').eq(row).find('td').eq(4).html(), 10);

	if (recept == "ANO") {
		var subsidy = parseInt($('table tbody tr').eq(row).find('td').eq(5).html(), 10);

		result_price = (basic_price - subsidy) * count;
	
		if (result_price < 0) {
			result_price = 0;
		}
	} else {
		result_price = basic_price * count;
	}
	
	$('table tbody tr').eq(row).find('td').eq(6).html(result_price + " Kč");
}

// valid input
var valid = function (row) {
	var check_recept = ( $("#recept_check").is(':checked') ) ? "ANO" : "NE";

	if (selected_drog_info.recept == "ANO" && check_recept == "NE") {
		// error dialog
		alert("Chyba! Nelze vydat lék, který je pozuze na předpis bez předpisu");
		return false;
	}

	if (selected_drog_info.recept == "ANO" && (id_insurance_selected == null || id_insurance_selected == "null")) {
		// error dialog
		alert("Chyba! Nelze vydat lék, který je pozuze na předpis bez zvolení zdravotní pojišťovny");
		return false;
	}

	if (check_recept == "ANO" && (id_insurance_selected == null || id_insurance_selected == "null")) {
		// error dialog
		alert("Chyba! Nelze vydat lék s předpisem, bez vybraní pojišťovny.");
		return false;
	}

	var drog_want_text = $('#drog_count').val();
	//if (!(drog_want_text.match(/^\d+$/) || parseInt(drog_want_text, 10) < 1)) { 
	if (parseInt(drog_want_text, 10) < 1 || (!(drog_want_text.match(/^\d+$/)))) { 
		// error dialog
		alert("Chyba! Chybný formát počtu kusů.");
		return false;
	}

	var drog_on_shop = parseInt($('#count_elm').text(), 10);
	var drog_want = parseInt($('#drog_count').val(), 10);

	if (drog_want > drog_on_shop) {
		// error dialog
		alert("Chyba! Nedostatečný počet léků na této pobočce.");
		return false;
	}

	return true;
}

// calculate and uppdate total sum under table
var updateTotalSum = function () {
	var row_count = $('#table tbody tr').length;
	var total_sum = 0;
	var cur_sum = 0;
	for (var i = 0; i < row_count; i++) {
		cur_sum = parseInt($('table tbody tr').eq(i).find('td').eq(6).html(), 10);
		total_sum += cur_sum;
	}

	$("#total_sum").text(total_sum + " Kč");
}

// carry out total reset of all
var totalReset = function () {
	cleaneSelect();
	rowCount = 0;
	$("#table tbody").empty();
	$("#total_sum").text(0 + " Kč");
	id_insurance_selected = "null";
	$("#insurances").val(id_insurance_selected);
	$('#insurance_selected').html("Zákazníkova pojišťovna: žádná");
	selected_drog_info.name = "";
	selected_drog_info.recept = false;
	selected_drog_info.id_drog = [];
	selected_drog_info.basic_price = "";
	selected_drog_info.EAN = "";
}

// click on cancel button under table
$(function() {
	$("#cancel_btn").click(function() {
		totalReset();
	});
});

// click on confirm button under table and send one purchase
$(function() {
	$("#confirm_btn").click(function() {
		// prepare result
		var row_count = $('#table tbody tr').length;
		if (row_count == 0) {
			// error dialog
			alert("Chyba! Nákup musí obsahovat alespoň jeden lék.");
			return;
		}
		// check length
		var result = {
			"id_pojistovna" : ($("#insurances").val() == "null" ? null : $("#insurances").val()),
			"lek" : []
		}
		// get all data from table
		var cur_recept = 0;
		var cur_arr;
		var one_drog;
		var cur_subsidy;
		for (var i = 0; i < row_count; i++) {
			cur_recept = ($('table tbody tr').eq(i).find('td').eq(2).html() == "ANO" ? true : false);
			cur_arr = ($('table tbody tr').eq(i).find('td').eq(7).html()).split(',').map(function(el){ return +el;});
			cur_subsidy = parseInt($('table tbody tr').eq(i).find('td').eq(5).html(), 10);
			
			for (var j = 0; j < cur_arr.length; j++) {
				one_drog = {"id_lek": "", "predpis": false, "prispeno": ""};
				one_drog.id_lek = cur_arr[j];
				one_drog.predpis = cur_recept;
				one_drog.prispeno = cur_subsidy;

				(result.lek).push(one_drog);
			}
		}

		// post total result
		$.ajax ({
			url: url + "/purchase",
			type: "POST",
			crossDomain: true,
			dataType: 'json',
			xhrFields: {
				withCredentials: true
			},
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(result),
			success: function(data, nothing, response) {
				if(response.status == 200) {
					var total_sum = $("#total_sum").text();
					alert("Nákup byl úspěšný a je zaevidován.\nVyberte od zákazníka " + total_sum + ".");

					// totoal reset clean
					totalReset();
				} else {
					alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
				}
			},
			error: function(data, nothing, response) {
				if (data.status == 401) {
					window.location.href="login.html";
				}
			}
		});
	});
});

// do not update page after click enter in input
$(document).on('keyup keypress', 'form input[type="text"]', function(e) {
  if(e.keyCode == 13) {
	e.preventDefault();
	return false;
  }
});

