/**
 * @description JS for insurance.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 26.11.2017
 */

var url = conf.url;

// async get for check get All Insurance
var getAllInsurance = function () {
	$.ajax ({
		url: url + "/insurance",
		type: "GET",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		success: function(insurance_company, nothing, response) {
			if(response.status == 200) {

				for (var i = 0; i < insurance_company.length; i++) {
					$('#insurances')
					.append($("<option></option>")
					.attr("value", insurance_company[i].id_pojistovna)
					.text(insurance_company[i].nazev_pojistovny)); 
				}

			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
			else if (data.status == 403) {
				window.location.href="index.html";
			}
		}
	});
};
getAllInsurance();

// select the insurance company and write its to the label top on drog list
$(function() {
	$( "select" )
	.change(function () {
		var insurance_id = ($("#insurances").val() == null ? 1 : $("#insurances").val());

		var str = "VZP";
		$("select option:selected").each(function() {
			str = $(this).text();
			insurance_id = $(this).val();
		});

		$('#insurance_selected').html("pojišťovna, pro kterou proběhne aktualizace: " + str);

		checkActualSubsidy(selected_drog_info.EAN);

	})
	.change();
});

var drogs; // all lek_info

// selected drog
var selected_drog_info = {
	"name": "",
	"recept": false,
	"id_drog": [],  // array of lek_id from selected lek_info
	"basic_price": "",
	"EAN": "",
	"type": ""
};

// this is sync, because we do not work until are all downloaded
$.ajaxSetup({
	async: false, 
	xhrFields: { 
		withCredentials:true 
	}, 
	crossDomain: true
});
$.get(url, function (data) {
	drogs = data;
});

// auto complete search box
$(function() {
	// setup autocomplete function pulling from drogs[] array
	$('#autocomplete').autocomplete({
		source: drogs,
		select: function (event, ui ) {
			var thehtml = '<strong>Název léku:</strong> ' + ui.item.nazev + 
			' <br> <strong>EAN:</strong> <p id="EAN_el" style="display:inline">' + ui.item.id_lek_info + "</p>" +
			' <br> <strong>Typ léku:</strong> ' + ui.item.typ_leku +
			' <br> <strong>Pouze na předpis:</strong> ' + (ui.item.je_na_predpis == "1" ? "ANO" : "NE") +
			' <br> <strong>Prodejní cena:</strong> <h2 style="margin-top: 3px;">' + ui.item.prodejni_cena + " Kč</h2>";

			$('#outputbox').show();

			$('#outputcontent').html(thehtml);
			checkActualSubsidy(ui.item.id_lek_info);
			
			selected_drog_info.name = ui.item.nazev;
			selected_drog_info.type = ui.item.typ_leku;
			selected_drog_info.basic_price = ui.item.prodejni_cena;
			selected_drog_info.EAN = ui.item.id_lek_info;
			selected_drog_info.recept = (ui.item.je_na_predpis == "1" ? "ANO" : "NE");
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
			else if (data.status == 403) {
				window.location.href="index.html";
			}
		}
	});	 
});

// async post for check actual subsidy
var checkActualSubsidy = function (id_lek_info) {
	// empty
	if (id_lek_info == "") {
		return;
	}

	var insurance_id = ($("#insurances").val() == null ? 1 : $("#insurances").val());

	$.ajax ({
		url: url + "/subsidy",
		type: "POST",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({"id_lek_info": id_lek_info, "id_pojistovna": insurance_id}),
		success: function(data, nothing, response) {
			if (response.status == 200) {

				var cur_subsidy = data[0].prispovana_cena;

				$('#subsidy').html("<strong>Aktulní výše příspěvku: </strong><h4 style='display:inline' id='subsidy_elm'>" + cur_subsidy + " Kč</h4>");
				selected_drog_info.id_drog = data.id_lek;

			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
			else if (data.status == 403) {
				window.location.href="index.html";
			}
		}
	});
};

// click on add btn
$(function() {
	$("#add").click(function() {
		if (valid()) {
			addToDrogLists();
			cleaneSelect();
		}
	});
});

// fce for add new subsidy to list table
var addToDrogLists = function () {
	var newRowContent = "<tr>";
	newRowContent += "<td>" + selected_drog_info.EAN + "</td>";
	newRowContent += "<td>" + selected_drog_info.name + "</td>";
	newRowContent += "<td>" + selected_drog_info.type + "</td>";
	newRowContent += "<td>" + $("#drog_count").val() + " Kč</td>";
	newRowContent += "</tr>";

	// add row to table
	$("#table tbody").append(newRowContent);
}

// reset and clean after add drog
var cleaneSelect = function () {
	$("#autocomplete").val("");
	$("#drog_count").val("0");
	// add row to table
	$('#outputbox').hide();
}

// carry out total reset of all
var totalReset = function () {
	cleaneSelect();
	$("#table tbody").empty();
	selected_drog_info.name = "";
	selected_drog_info.id_drog = [];
	selected_drog_info.basic_price = "";
	selected_drog_info.EAN = "";
}

// click on cancel button under table
$(function() {
	$("#cancel_btn").click(function() {
		totalReset();
	});
});

// valid input
var valid = function (row) {
	var new_subsidy = $('#drog_count').val();

	// bad format of input subsidy
	if (parseInt(new_subsidy, 10) < 0 || (!(new_subsidy.match(/^\d+$/)))) { 
		// error dialog
		alert("Chyba! Chybný formát nového příspěvku.");
		return false;
	}

	// repeate the same drog
	var row_count = $('#table tbody tr').length;
	var EAN_current = $('#EAN_el').text();
	var EAN_table;
	for (var i = 0; i < row_count; i++) {
		EAN_table = $('table tbody tr').eq(i).find('td').eq(0).html();

		if (EAN_table == EAN_current) {
			alert("Chyba! Nelze vložit vícekrát nový příspěvek na lék.");
			return false;
		}
	}

	return true;
}

// do not update page after click enter in input
$(document).on('keyup keypress', 'form input[type="text"]', function(e) {
	if(e.keyCode == 13) {
		e.preventDefault();
		return false;
	}
});

// click on confirm button under table and send one purchase
$(function() {
	$("#confirm_btn").click(function() {
		// prepare result
		var row_count = $('#table tbody tr').length;
		if (row_count == 0) {
			// error dialog
			alert("Chyba! Import musí obsahovat alespoň jeden lék.");
			return;
		}
		// check length
		var result = {
			"id_pojistovna" : $("#insurances").val(),
			"lek" : []
		}

		// get all data from table
		var one_drog;
		for (var i = 0; i < row_count; i++) {
			one_drog = {"id_lek_info": "", "subsidy": ""};
			one_drog.id_lek_info = $('table tbody tr').eq(i).find('td').eq(0).html();
			one_drog.subsidy = parseInt($('table tbody tr').eq(i).find('td').eq(3).html(), 10);
			(result.lek).push(one_drog);
		}

		// post total result
		$.ajax ({
			url: url + "/subsidy/update",
			type: "POST",
			crossDomain: true,
			dataType: 'json',
			xhrFields: {
				withCredentials: true
			},
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify(result),
			success: function(data, nothing, response) {
				if(response.status == 200) {
					// msg to client
					alert("Import léků proběhl úspěšně");

					// totoal reset clean
					totalReset();
				} else {
					alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
				}
			},
			error: function(data, nothing, response) {
				
				if (data.status == 401) {
					window.location.href="login.html";
				}
				else if (data.status == 403) {
					window.location.href="index.html";
				}
			}
		});
	});
});
