/**
 * @description JS for record.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 26.11.2017
 */

var url = conf.url;
	
// async get for check get All Insurance
var getAllRecords = function () {
	$.ajax ({
		url: url + "/records",
		type: "POST",
		xhrFields: { withCredentials:true },
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		//data: JSON.stringify({"id_sklad": 1}),
		data: JSON.stringify({}),
		success: function(data, nothing, response) {
			if(response.status == 200) {

				var result_price;
				for (var i = 0; i < data.length; i++) {
					
					result_price = parseInt(data[i].prodejni_cena, 10) - (data[i].prispeno == null ? 0 : parseInt(data[i].prispeno, 10));

					// prepare html to table
					var newRowContent = "<tr><td>" + myParseTime(data[i].cas_prodeje) + "</td><td>" + data[i].id_lek_info + "</td><td>" + data[i].id_lek;
					newRowContent += "</td><td>" + data[i].nazev + "</td><td>" + data[i].typ_leku + "</td><td>" + (data[i].id_pojistovna == null ? "NE" : "ANO");
					newRowContent += "</td><td>" + (data[i].nazev_pojistovny == null ? "" : data[i].nazev_pojistovny) + "</td><td>";
					newRowContent += parseInt(data[i].prodejni_cena, 10) + " Kč</td><td>" 
					newRowContent += (data[i].prispeno == null ? 0 : parseInt(data[i].prispeno, 10)) + " Kč</td><td>";
					newRowContent += result_price + " Kč</td></tr>";

					// add row to table
					$("#table tbody").append(newRowContent);
				}

				updateTotalSum();
			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
		}
	});
};
getAllRecords();

// calculate and uppdate total sum top on table
var updateTotalSum = function () {
	var row_count = $('#table tbody tr').length;
	var total_sum = 0;
	var cur_sum = 0;
	for (var i = 0; i < row_count; i++) {
		cur_sum = parseInt($('table tbody tr').eq(i).find('td').eq(9).html(), 10);
		total_sum += cur_sum;
	}

	$("#total_sum").text(total_sum + " Kč");
}

// parse ISO time to more user friendly string
var myParseTime = function (time) {
	time = time.substring(0, time.length - 5);
	time = time.split('T').join(" ");
	return time;
}
