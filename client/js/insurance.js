/**
 * @description JS for insurance.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 26.11.2017
 */

var url = conf.url;

// async get for check get All Insurance
var getAllInsurance = function () {
	$.ajax ({
		url: url + "/insurance",
		type: "GET",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		success: function(insurance_company, nothing, response) {
			if(response.status == 200) {

				for (var i = 0; i < insurance_company.length; i++) {
					$('#insurances')
					.append($("<option></option>")
					.attr("value", insurance_company[i].id_pojistovna)
					.text(insurance_company[i].nazev_pojistovny)); 
				}

			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			if (data.status == 401) {
				window.location.href="login.html";
			}
			else if (data.status == 403) {
				window.location.href="index.html";
			}
		}
	});
};
getAllInsurance();

// select the insurance company and write its to the label top on drog list
$(function() {
	$( "select" )
	.change(function () {
		var insurance_id = ($("#insurances").val() == null ? 1 : $("#insurances").val());
		$("#table tbody").empty();
		getAllInsuranceExport(insurance_id);
	})
	.change();
});

// async get for check get All Insurance
var getAllInsuranceExport = function (insurance_id) {
	$.ajax ({
		url: url + "/insurance",
		type: "POST",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			"id_pojistovna": insurance_id,
			"from": "2000-01-30", // TODO functionality on web, where user will be select from and to 
			"to" : "2050-01-30" // TODO functionality on web, where user will be select from and to 
			// "branch": *  // TODO add functionality to web, where user will be select all branch
		}),
		success: function(data, nothing, response) {
			if(response.status == 200) {

				var result_price;
				for (var i = 0; i < data.length; i++) {
					
					var newRowContent = "<tr>";
					newRowContent += "<td>" + data[i].id_lek_info + "</td>";
					newRowContent += "<td>" + data[i].nazev + "</td>";
					newRowContent += "<td>" + data[i].typ_leku + "</td>";
					newRowContent += "<td>" + data[i].count + "</td>";
					newRowContent += "<td>" + data[i].prispeno + " Kč</td>";
					newRowContent += "<td>" + (parseInt(data[i].count, 10) * parseInt(data[i].prispeno, 10)) + " Kč</td>";
					newRowContent += "</tr>";

					// add row to table
					$("#table tbody").append(newRowContent);
				}
				updateTotalSum();
			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			
			if (data.status == 401) {
				window.location.href="login.html";
			}
			else if (data.status == 403) {
				window.location.href="index.html";
			}
		}
	});
};

// calculate and uppdate total sum top on table
var updateTotalSum = function () {
	var row_count = $('#table tbody tr').length;
	var total_sum = 0;
	var cur_sum = 0;
	for (var i = 0; i < row_count; i++) {
		cur_sum = parseInt($('table tbody tr').eq(i).find('td').eq(5).html(), 10);
		total_sum += cur_sum;
	}

	$("#total_sum").text(total_sum + " Kč");
}
