/**
 * @description JS for all html, valid permission
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 30.11.2017
 */

var url = conf.url;

// async get for get user name and permission (role)
var getUser = function () {
	$.ajax ({
		url: url + "/user",
		type: "GET",
		xhrFields: { withCredentials:true },
		dataType: "json",
		contentType: "application/json; charset=utf-8",
		success: function(data, nothing, response) {
			if(response.status == 200) {
				$("#user_id").text(data.user + " |");
				
				// show admin's btns?
				if (data.role == "admin") {
					$('#sub_li').show();
                    $('#ins_li').show();
                    $('#usr_li').show();
					$('#edit_li').show();
				}

			} else {
				alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
			}
		},
		error: function(data, nothing, response) {
			if (data.status == 401) {
				window.location.href="login.html";
			}
		}
	});
};
getUser();

// after 5 minute without mousemove or key press logout user
var idleTime = 0;
$(document).ready(function () {
	// Increment the idle time counter every minute.
	var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

	// Zero the idle timer on mouse movement.
	$(this).mousemove(function (e) {
		idleTime = 0;
	});
	$(this).keypress(function (e) {
		idleTime = 0;
	});
});

// increment. after 5 minute without mousemove or key press logout user
function timerIncrement() {
	idleTime = idleTime + 1;
	if (idleTime > 4) { // 5 minutes
		window.location.href="login.html";
	}
}
