/**
 * @description JS for edituser.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 1.12.2017
 */

var url = conf.url;

before_users = [];
after_users = [];

// async get for check get All Insurance
var getAllUsers = function () {
    $.ajax ({
        url: url + "/user/all",
        type: "GET",
        crossDomain: true,
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        contentType: "application/json; charset=utf-8",
        success: function(data, nothing, response) {
            if(response.status == 200) {


                for (var i = 0; i < data.length; i++) {
                    before_users.push(data[i].id_uzivatel);
                }

                // cur user first
                var cur_user = $("#user_id").text();
                cur_user = cur_user.substring(0, cur_user.length - 2);
                for (var i = 0; i < data.length; i++) {
                    
                    if (data[i].jmeno == cur_user) {
                        $("#id").val(data[i].id_uzivatel);
                        $("#user").val(data[i].jmeno);
                        $("#user_role").val(data[i].role);
                        $('#user_role').prop('disabled', true);

                        data.splice(i, 1);
                    }
                }

                // other users
                for (var i = 0; i < data.length; i++) {
                    addUser(data[i].id_uzivatel, data[i].jmeno, data[i].role);
                }

            } else {
                alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
            }
        },
        error: function(data, nothing, response) {
            if (data.status == 401) {
                window.location.href="login.html";
            }
            else if (data.status == 403) {
                window.location.href="index.html";
            }
        }
    });
};
getAllUsers();


var id_count = 1;
var addUser = function (id, user, role) {
	// Cloning Form
	var source = $('.form-holder:first'), clone = source.clone();

	clone.find(':input').attr('id', function(i, val) {
		return val + id_count;
	});

	clone.appendTo('.form-holder-append');
	
    $("#id" + id_count).val(id);
    $("#user" + id_count).val(user);
    $("#user_role" + id_count).val(role);
    $('#user_role' + id_count).prop('disabled', false);

	id_count++;
}


$(function() {
    $("#confirm_btn").click(function() {

        var result = [];
        var user;

        for (var i = 0; i < id_count; i++) {
            user = {
                "id": "", 
                "role": ""
            };

            if (i == 0) {
                user.id = $("#id").val();
                user.role = $("#user_role").val();
                after_users.push(user.id);
            } else {
                if ($("#pass" + i).val() != $("#pass_again" + i).val()) {
                    alert("Chyba. Hesla musí být shodná");
                    return;
                }
                user.id = $("#id" +i).val();
                user.role = $("#user_role" + i ).val();
                after_users.push(user.id);
            }
            result.push(user);
        }

        var del_users = diff(after_users, before_users);  

        update(result, del_users);
        
    });
});

// // help fce for diff two array
function diff (a1, a2) {
    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
};


// Removing Form Field
$(function() {
    $('body').on('click', '.remove', function() {
        var closest = $(this).closest('.form-holder').remove();
        id_count--;
    });
});

// cancel all - reload
$(function() {
    $("#cancel_btn").click(function() {
        window.location.href="edituser.html";
    });
});

// async update users
var update = function (data, rem) {
    // alert(JSON.stringify({"data": data}));

    $.ajax ({
        url: url + "/user/update",
        type: "POST",
        crossDomain: true,
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({"data": data, "remove": rem}),
        success: function(insurance_company, nothing, response) {
            if(response.status == 200) {

                alert("Aktualizace uživatelů proběhla úspěšně.");
                // reset
                window.location.href="edituser.html";

            } else {
                alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
            }
        },
        error: function(data, nothing, response) {
            if (data.status == 401) {
                window.location.href="login.html";
            }
            else if (data.status == 403) {
                window.location.href="index.html";
            }
        }
    });
};
