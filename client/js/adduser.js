var id_count = 1;

$(function() {

	// Cloning Form
	
	$('.add').on('click', function() {

		var source = $('.form-holder:first'), clone = source.clone();

		clone.find(':input').attr('id', function(i, val) {
			return val + id_count;
		});

		clone.appendTo('.form-holder-append');
		

        $("#user" + id_count).val("");
        $("#pass" + id_count).val("");
        $("#pass_again" + id_count).val("");


		id_count++;
	});
});


$(function() {
    $("#confirm_btn").click(function() {

        var result = [];
        var user;

        for (var i = 0; i < id_count; i++) {
            user = {
                "name": "", 
                "pass": "", 
                "role": ""
            };

            if (i == 0) {
                if ($("#pass").val() != $("#pass_again").val()) {
                    alert("Chyba. Hesla musí být shodná");
                    return;
                }

                user.name = $("#user").val();
                user.pass = $("#pass").val();
                user.role = $("#user_role").val();
            } else {
                if ($("#pass" + i).val() != $("#pass_again" + i).val()) {
                    alert("Chyba. Hesla musí být shodná");
                    return;
                }
                user.name = $("#user" + i).val();
                user.pass = $("#pass" +i).val();
                user.role = $("#user_role" + i ).val();
            }
            result.push(user);
        }

        // TODO send
        // alert(JSON.stringify(result, null, 4));

        add(result);

    });
});

// async update users
var add = function (data) {
    // alert(JSON.stringify({"data": data}));
    $.ajax ({
        url: url + "/user/add",
        type: "POST",
        crossDomain: true,
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({"data": data}),
        success: function(insurance_company, nothing, response) {
            if(response.status == 200) {

                alert("Uživatelé byly úspěšně přidání do systému. Budete přesměrování na přehled uživatelů, kde je můžete případně editovat.");
                // redirect
                window.location.href="edituser.html";

            } else {
                alert("Došlo k neočekávané chybě na serveru:\n" + response.message);
            }
        },
        error: function(data, nothing, response) {
            if (data.status == 401) {
                window.location.href="login.html";
            }
            else if (data.status == 403) {
                window.location.href="index.html";
            }
        }
    });
};
