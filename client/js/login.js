/**
 * @description JS for login.html
 * @author Cyril Urban <cyril.urban.cu@gmail.com>
 * date 26.11.2017
 */

var url = conf.url;

// log out user
var logout = function () {
	// logout then login page
	$.ajax ({
		url: url + "/login/logout",
		type: "GET",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		success: function(data, nothing, response) {
			// alert("logout success");
		}
	});
}
// auto call when load this page
logout();

// login user
var login = function () {
	// get data
	var result = {
		"user" : $("#user").val(),
		"password" : $("#password").val()
	}

	// post data
	$.ajax ({
		url: url + "/login",
		type: "POST",
		crossDomain: true,
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(result),
		success: function(data, nothing, response) {
			if(response.status == 200) {
				// alert(JSON.stringify(data, null, 4));
				window.location.href="index.html";
			}
		},
		error: function(data, nothing, response) {
			alert(data.responseText);
		}
	});
}

// click on login button
$(function() {
	$("#login").click(function() {
		login();
	});
});


// click on enter in password input
$(document).on('keyup keypress', 'form input[type="password"]', function(e) {
	if(e.keyCode == 13) {
		login();
	}
});